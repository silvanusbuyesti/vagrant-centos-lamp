#Basic Vagrant CentOS/LAMP setup

This is a generic setup of a CentOS install using Vagrant.  It is based on scalefactory/centos vagrant box.

Some changes/additions to how the install is setup can be done in the bootstrap.sh file.

The virtualhost.txt file contains a default setup for a website called mysite.com.

In order to have your browser route automatically to the site specified, it will need to be added to your /etc/hosts file.

Defaults to setup:

* Live site files - public/mySite/public_html
* root password - vagrant
* mysql default - user:root password:vagrant

In order to not have to refactor files once moving to live hosting account, the virtualhosts.txt and Vagrantfile have
been set up to be able to use a specified username.  Simply replace 'REPLACEUSERHERE' with the web hosting account
username.  However, you may still have to update the 'home' in your files, once moved live.
