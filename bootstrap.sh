#!/bin/bash
yum install -y httpd httpd-devel mysql mysql-server mysql-devel php php-mysql php-common php-gd php-mbstring php-bcrypt php-mcrypt php-devel php-xml phpmyadmin

APACHECONFIG=/etc/httpd/conf/httpd.conf
VIRTUAL=/vagrant/virtualhost.txt
PHPMYADMINCONFIG=/etc/httpd/conf.d/phpMyAdmin.conf

cat "$VIRTUAL" >> "$APACHECONFIG"

sed -i "/AllowOverride None/c AllowOverride All" $APACHECONFIG

sed -i "/Deny from All/s/^/#/" $PHPMYADMINCONFIG

yum update -y

/etc/init.d/httpd start
/etc/init.d/mysqld start

mysqladmin -u root password vagrant

chkconfig httpd on
chkconfig mysqld on
